rails g scaffold Employee name:string gender:boolean age:integer address:string phone_number:string
rails g scaffold Customer name:string address:string phone_number:string
rails g scaffold Invoice employee:references customer:references
rails g scaffold Product name:string price:float
rails g scaffold LineItem invoice:reference product:reference
