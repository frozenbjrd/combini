class Invoice < ApplicationRecord
  belongs_to :employee
  belongs_to :customer

  has_many :line_items
end
