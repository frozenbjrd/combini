class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.boolean :gender
      t.integer :age
      t.string :address
      t.string :phone_number

      t.timestamps
    end
  end
end
