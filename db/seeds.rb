require 'faker'

def seed_customer
  Customer.create(name: Faker::Name.name, address: Faker::Address.full_address, phone_number: Faker::PhoneNumber.phone_number)
end

def seed_employee
  Employee.create(name: Faker::Name.name, age: rand(18..700), gender: true)
end

def seed_product
  Product.create(name: Faker::ProgrammingLanguage.name, price: Faker::Number.decimal(l_digits: 2))
end

def seed_invoice
  Invoice.create(employee: Employee.find(Employee.pluck(:id).shuffle.first), customer: Customer.find(Customer.pluck(:id).shuffle.first))
end

def seed_line_item
  Invoice.all.each do |invoice|
    total = 0
    rand(1..10).times do 
      invoice.line_items.create(product: Product.find(Product.pluck(:id).shuffle.first), quantity: rand(1..10)) 
      total += invoice.line_items.last.product.price * invoice.line_items.last.quantity
    end
    invoice.update(total: total)
  end
end

rand(1..100).times { seed_customer }
rand(1..100).times { seed_employee }
rand(1..100).times { seed_product }
rand(1..100).times { seed_invoice }
seed_line_item
