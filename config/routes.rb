Rails.application.routes.draw do
  root to: 'products#index'
  resources :line_items
  resources :products
  resources :invoices
  resources :customers
  resources :employees

  namespace :admin do
      resources :customers
      resources :employees
      resources :invoices
      resources :line_items
      resources :products

      root to: "customers#index"
    end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
