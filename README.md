# Combini

An admin dashboard to manage a convenient store (or combini in Japan)

# Technical requirement

Ruby v.5.8 is needed to run this project. It is recommended to be installed via [Ruby Version Manager](https://rvm.io/). Other dependancies can be found in Gemfile.

# Run on localhost

Make sure you are open the folder `combini` in the terminal.  

* Install dependancies: `bundle update && bundle install`
* Create database: `rails db:create && rails db:migrate`
* Populate database: `rails db:seed`
* Run the server `rails server`

Then go to [localhost:3000/admin](http://localhost:3000/admin) to see the dashboard.

# Features

Support basic actions on models: create, edit, update, delete.

## Use case
As employee, I want to create a new invoice (bill) when customer purchase a product. Steps are follow:
  
- Create new invoice with employee id and customer id.  
- Create new line item with product id and quantity (ex: Python, 3).
- Update invoice with with line item.
- Update total price (product's price * quantity)

